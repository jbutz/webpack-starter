var path = require('path');
var webpack = require('webpack');
var HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  entry: './src/index.js',
  output: {
    filename: 'bundle.js',
    path: path.resolve(__dirname, 'dist')
  },
  resolve: {
    alias: {
      'history.js$': path.resolve(__dirname, 'node_modules/history.js/history.js'),
    }
  },
  devtool: "cheap-eval-source-map",
  plugins: [
    new HtmlWebpackPlugin({
      template: './src/index.ejs'
    }),
    new webpack.ProvidePlugin({
      $: "jquery",
      jQuery: "jquery",
      ko: 'knockout',
      pager: 'pagerjs'
    })
  ]
};
